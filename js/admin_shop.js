/* 자주쓰는 정보 아이템 삭제 */
 $('.list_favorit_item .btn_del').on('click', function(){
    $(this).closest('li').remove();
});

/* 배송 및 택배설정 */
var $ShppingMethod = $(document.createDocumentFragment());
var objShppingMethod = ['택배', '직접배송(화물 배달)', '방문수령', '퀵서비스', '배송없음'];
objShppingMethod.forEach(function(item,i) {
    $ShppingMethod.append('<option value="shipping' + i + '">' + item + '</option>');
});
$('#select_shpping').append($ShppingMethod);

/* 배송비 결제방법 */
$Payment_method = $(document.createDocumentFragment());
var objPayment_method = ['선결제', '착불'];
objPayment_method.forEach(function(item,i) {
    $Payment_method.append('<option value="payment' + i + '">' + item + '</option>');
});
$('#select_payment').append($Payment_method);

/* 택배사 선택 */
$Select_courier = $(document.createDocumentFragment());
var objcourier = 
['없음', 'CJ대한통운', '우체국 택배', '우편등기','한진택배','로젠택배',
'롯데택배','대신택배','일양로지스','경동택배','농협택배','세방택배',
'천일택배', 'CVSnet 편의점 택배', '합동택배', 'EMS', 'K-Packet', 
'DHL', 'Fedex', 'GSMNtoN', 'UPS', 'TNT Express', 'USPS', '에어보이익스프레스',
'DHL Global Mail', 'i-Parcel', '한의사랑택배', '건영택배', '범한판토스', '굿투럭','DHL 독일', 'ACI', 
'롯데택배(국제택배)', 'CJ대한통운(국제택배)', '성원글로벌', '대운글로벌',
'SLX택배', '호남택배','GSI익스프레스','KGB택배', 'CU편의점택배', '용마로지스', 
'홈픽택배', '라인익스프레스', 'GPS LOGIX', '시알로지텍','브릿지르지스','EFS','기타택배'];

objcourier.forEach(function(item,i) {
    $Select_courier.append('<option value="courier' + i + '">' + item + '</option>');
});
$('#select_courier').append($Select_courier);



/* 배송비 선택 */

var ShippingfeeType1 = '<div class="type type1">';
    ShippingfeeType1 +=     '<span class="txt">기본 배송비</span>'
    ShippingfeeType1 +=     '<input type="text" class="inp_write_price" value="2500" />'
    ShippingfeeType1 +=     '<span class="txt_price">원</span>';
    ShippingfeeType1 += '</div>';

var ShippingfeeType2 = '<div class="type type2">';
    ShippingfeeType2 +=     '<div class="mb17">';
    ShippingfeeType2 +=         '<span class="txt">기본 배송비</span>';
    ShippingfeeType2 +=         '<input type="text" class="inp_write_price" value="2500" />';
    ShippingfeeType2 +=         '<span class="txt_price">원</span>';
    ShippingfeeType2 +=     '</div>';
    ShippingfeeType2 +=     '<div>';
    ShippingfeeType2 +=         '<span class="txt">구매금액 합계</span>';
    ShippingfeeType2 +=         '<input type="text" class="inp_write_price" value="5000" />';
    ShippingfeeType2 +=         '<span class="txt_price">원 이상 무료배송</span>';
    ShippingfeeType2 +=      '</div>';
    ShippingfeeType2 +=  '</div>';  

var ShippingfeeType3 =   '<div class="type type3">';
    ShippingfeeType3 +=  '    <span class="txt"></span>';
    ShippingfeeType3 +=  '    <input type="text" class="inp_write_price" value="1" />';
    ShippingfeeType3 +=  '    <span class="txt_price">개 이상 구매시</span>';
    ShippingfeeType3 +=  '    <input type="text" class="inp_write_price ml10" value="N" />';
    ShippingfeeType3 +=  '    <span class="txt_price">원</span>';
    ShippingfeeType3 +=  '</div>';
   

var ShippingfeeType4 =   '<div class="type type4">'; 
    ShippingfeeType4 +=  '    <span class="txt"></span>'; 
    ShippingfeeType4 +=  '    <input type="text" class="inp_write_price" value="N" />'; 
    ShippingfeeType4 +=  '    <span class="txt_price  w75">kg부터</span>'; 
    ShippingfeeType4 +=  '    <input type="text" class="inp_write_price ml25" value="N" />'; 
    ShippingfeeType4 +=  '    <span class="txt_price">원</span>'; 
    ShippingfeeType4 +=  '</div>'; 

var ShippingfeeType5 =   '<div class="type type5">'; 
    ShippingfeeType5 +=  '    <span class="txt">구매금액 합계</span>'; 
    ShippingfeeType5 +=  '    <input type="text" class="inp_write_price" value="N" />'; 
    ShippingfeeType5 +=  '    <span class="txt_price  w75">원 부터</span>'; 
    ShippingfeeType5 +=  '    <input type="text" class="inp_write_price ml25" value="N" />'; 
    ShippingfeeType5 +=  '    <span class="txt_price">원</span>'; 
    ShippingfeeType5 +=  '</div>    ';

/* 배송비 */
$Shippingfee = $(document.createDocumentFragment());
var objShippingfee = ['고정배송비', '조건부 무료배송', '무게별 차등배송', '수량별 차등 배송', '구매액별 차등배송', '무료배송'];
objShippingfee.forEach(function(item,i) {
    $Shippingfee.append('<option value="shippingfee' + i + '">' + item + '</option>');
});
$('#select_shippingfee').append($Shippingfee);

function contShippingfeeInit() {
    $('#js-type_shipitem').append(ShippingfeeType1);
}
contShippingfeeInit();


function shippingfeeADD(value) {
    if (value == 'shippingfee0') {$('#js-type_shipitem').append(ShippingfeeType1);}
    if (value == 'shippingfee1') {$('#js-type_shipitem').append(ShippingfeeType2);}
    if (value == 'shippingfee2') {$('#js-type_shipitem').append(ShippingfeeType3);}
    if (value == 'shippingfee3') {$('#js-type_shipitem').append(ShippingfeeType4);}
    if (value == 'shippingfee4') {$('#js-type_shipitem').append(ShippingfeeType5);}
    return value;
}

$("#select_shippingfee").change(function(){ 
    var value = $(this).val();
    $('#js-type_shipitem').empty();
    shippingfeeADD(value);    
    if (value == 'shippingfee2' || value == 'shippingfee3' || value == 'shippingfee4') {$('#js-notice, #js-add_delivery').show();} 
    else {$('#js-notice, #js-add_delivery').hide();}
});

$('#js-add_delivery').on('click', function(){
    var value = $('#select_shippingfee option:selected').val();
    shippingfeeADD(value);
});




/* 지역별 배송비 */
$localshippingfee = $(document.createDocumentFragment());
var objlocalshippingfee = ['지역별 배송비 사용', '지역별 배송비 사용안함'];
objlocalshippingfee.forEach(function(item,i) {
    $localshippingfee.append('<option value="localshippingfee' + i + '">' + item + '</option>');
});
$('#select_localshippingfee').append($localshippingfee);