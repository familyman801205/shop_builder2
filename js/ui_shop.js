$(document).on('mousedown', '.special_shop ', function (e) {
    e.stopPropagation();
    rightMenuSpecialShop($(this));
}).on('mousedown', '.section_product_detail ', function (e) {
    e.stopPropagation();
    rightMenuDetailPage($(this));
});


/* 특별전 */
function rightMenuSpecialShop(el) {
    event.stopPropagation();
    if ((event.button == 2) || (event.which == 3)) {
        $('.right-click-layer').remove();
        el.prepend('<div class="right-click-layer" data-include="include/right_menu.html .right_menu18"></div>');
        rightMenuSettion(el);
    }
}

function PopSpecialShop(el) {
    var $specialShopTarget = el.closest('.special_shop');
    $(".pop_area").load("include/popup.html #shop_special_item", function () {
        $('.mask').show();
        specialShopElSetting($specialShopTarget);
    });
}
function specialShopElSetting(SpecialShopTarget) {
    $('#shop_special_item .btn_apply').on("click", function() {
        if ($('#dp_type1').is(":checked")) {
            $(SpecialShopTarget).removeClass('sty2');
        } else {
            $(SpecialShopTarget).addClass('sty2');           
        }
        $('.pop_product_display').remove();
        $('.mask').hide();
    });
}

/* 상세보기 페이지 */
function rightMenuDetailPage(el) {
    event.stopPropagation();
    if ((event.button == 2) || (event.which == 3)) {
        $('.right-click-layer').remove();
        el.prepend('<div class="right-click-layer" data-include="include/right_menu.html .right_menu19"></div>');
        rightMenuSettion(el);
    }
} 
function detailPageSetting(el) {
    var $specialShopTarget = el.closest('.section_product_detail');
    $(".pop_area").load("include/popup.html #pop_product_option", function () {
        $('.mask').show();
        detailPageElSetting($specialShopTarget);
    });
}
function detailPageElSetting() {
    $('#pop_product_option .btn_apply').on("click", function () {
        $('#pop_product_option').remove();
        $('.mask').hide();
    });
}

$('#btn_qa').on('click', function(){
    $(".pop_area").load("include/popup.html #pop_qna", function () {
        $('.mask').show();       
    });
});

$('.product_img_area .thumb_img img').on('click', function(){
    $('.full_img img').attr('src', $(this).attr('src')); 
});

function throttleUsingRaf(cb) {
  var rAfTimeout = null;

  return function () {
    if (rAfTimeout) {
      window.cancelAnimationFrame(rAfTimeout);
    }
    rAfTimeout = window.requestAnimationFrame(function () {
      cb();
    })
  }
}
function pageDetailonScroll() {
  // code ...
  let content = document.querySelector(".tab_bottom_detail");
    document.addEventListener("scroll", function() {
    var scrolled = document.scrollingElement.scrollTop;
    var position = content.offsetTop;
    if(scrolled > position){
        content.querySelector("#js-shopdetail-tab").classList.add('scroll_over');
        } else {
            content.querySelector("#js-shopdetail-tab").classList.remove('scroll_over');
        }
    });
}
if (document.querySelectorAll('.tab_bottom_detail').length != 0) {
    document.addEventListener('scroll', throttleUsingRaf(pageDetailonScroll));
}
const ProductTabMove = function (){
    var clickEl = $(this).index();
    $('.tab-titles li').removeClass('active');
    $(this).addClass('active');
    const TabScrollPos = function(targetEl) {$(window).scrollTop(targetEl.offset().top);}
    if (clickEl == 0) {$(window).scrollTop(TabScrollPos($('.product-detail')))} 
    if (clickEl == 1) {$(window).scrollTop(TabScrollPos($('.product-review')))} 
    if (clickEl == 2) {$(window).scrollTop(TabScrollPos($('.product-etc')))} 
    if (clickEl == 3) {$(window).scrollTop(TabScrollPos($('.product-qna')))}    
}
$('.tab-titles li').on('click', ProductTabMove);
$(document).on('click', '#pop_qna .btn_close, #pop_qna .btn_cancle', function(){$('#pop_qna').remove()});





/* 팝업 새로운 배송지 */
$(document).on('click', '.delivery_address_list li', function (param) {//배송지 하나 선택 효과
      $(this).addClass('active').siblings('li').removeClass('active');
    }).on('click', '.delivery_address_list .btn_del', function (param) {//배송지 하나 삭제
      $(this).parent('li').remove();
    }).on('click', '.tab_list li', function (param) {//최근배송지, 새로운배송지 탭 선택
      const getId = $(this).data('tab');
      $(this).addClass('active').siblings('li').removeClass('active');
      $('.tab_content').removeClass('active');
      $('#' + getId).addClass('active');
    }).on('click', '.select_contain', function (e) {//셀렉트 커스텀 목록 온오프
      e.stopPropagation();
      $('.select_contain').removeClass('on');
      $(this).addClass('on');
    }).on('click','.select_list li', function (e) {//셀렉트 목록 선택
      e.stopPropagation();
      const li_text = $(this).text();
      $(this).parent().prev('.selected_li').text(li_text);
      $('.select_contain').removeClass('on');
    });


/* 팝업 새로운 배송지 팝업불러오기 */
$('.btn_payment').on('click', function(){
   $(".pop_area").load("include/popup.html #pop_address", function () {
        $('.mask').show();       
    });
})
$(document).on('click', '.pop_address .btn_close', function(){
    $('.mask').hide();
    $('.pop_address').remove();
});
$(document).on('click', '.stars .icon_star', function(){
   var clickStarIdx = $(this).index();
    $('.stars .icon_star').removeClass('on');
   for (let i = 0; i <= clickStarIdx; i++) {
       $('.stars .icon_star').eq(i).addClass('on');       
   }
   if (clickStarIdx == 0) {$('.txt_point').text('아주 나쁨');}
   if (clickStarIdx == 1) {$('.txt_point').text('나쁨');}
   if (clickStarIdx == 2) {$('.txt_point').text('보통');}
   if (clickStarIdx == 3) {$('.txt_point').text('좋음');}
   if (clickStarIdx == 4) {$('.txt_point').text('아주 좋음');}

});

/* 구매후기 */
$('#js-btn-review').on('click', function(){
    $(".pop_area").load("include/popup.html #pop_review", function () {
        $('.mask').show();
    });
});


$(document).on('click', '.btn_del_file', function(){
        $(this).closest('.file_one').remove();
});
$(document).on('change', "#review_file", handleImgFileSelect);
function handleImgFileSelect(e) {        
    // 이미지 정보들을 초기화
    sel_files = [];
    var files = e.target.files;
    var filesArr = Array.prototype.slice.call(files);
    var index = 0;

    filesArr.forEach(function (f) {
        if (!f.type.match("image.*")) {
            alert("확장자는 이미지 확장자만 가능합니다.");
            return;
        }
        sel_files.push(f);
        var reader = new FileReader();
        reader.onload = function (e) {
            var addThumb = '<span class="file_one">'
            addThumb +=        '<img src="' + e.target.result + '" alt="" class="file_one_img" /><i class="btn_del_file"><span class="blind">업로드한 이미지 삭제하기</span></i>'
            addThumb += '   </span>' 
            if ($('.file_one_img').length < 8) {              
                $(".box_uploaded").append(addThumb);   
            } else {
                alert('이미지는 최대 8개까지 첨부가능합니다.')
            }              
            index++;
        }
        reader.readAsDataURL(f);
    });
    $('#input_imgs').val('');
}

$(document).on('click', '.wrap_mall_pop .btn_close, .wrap_mall_pop .btn_cancle', function(){
      $('.wrap_mall_pop').remove();
      $('.mask').hide();
});